#pragma checksum "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e68f9288c77f86fb06b58b643154f88c1c590c08"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Group_Index), @"mvc.1.0.view", @"/Views/Group/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Group/Index.cshtml", typeof(AspNetCore.Views_Group_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\_ViewImports.cshtml"
using groupcloud;

#line default
#line hidden
#line 2 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\_ViewImports.cshtml"
using groupcloud.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e68f9288c77f86fb06b58b643154f88c1c590c08", @"/Views/Group/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cf63df59a6290951308b08e2f1d065e97573d2c2", @"/Views/_ViewImports.cshtml")]
    public class Views_Group_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Group/LeaveGroup"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("Group/NewGroup"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 2 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/layout.cshtml";
    ViewBag.newGroupLink = true;

#line default
#line hidden
            BeginContext(117, 196, true);
            WriteLiteral("\n<dialog id=\"leaveGroup\" class=\"leaveGroup\">\n    <span class=\"title\">Czy jesteś pewien?</span>\n    <p>Jeśli opuścisz grupę, ponowne dołączenie do niej będzie wymagać ponownego zaproszenia</p>\n    ");
            EndContext();
            BeginContext(313, 183, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8de477ad1629488c8eb18970b950e4f7", async() => {
                BeginContext(360, 129, true);
                WriteLiteral("\n        <input type=\"hidden\" name=\"sGroupId\" id=\"groupId\">\n\n        <button class=\"btn btn-dialog btn-right\">Opuść</button>\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(496, 250, true);
            WriteLiteral("\n\n    <a href=\"#\" onclick=\"closeDialog(\'leaveGroup\')\" class=\"close-button\"><span class=\"fas fa-times\"></span></a>\n</dialog>\n\n<dialog id=\"newGroup\" class=\"newGroup\">\n    <span class=\"title\">Nowa grupa</span>\n    <p>Wpisz nazwę tworzonej grupy</p>\n    ");
            EndContext();
            BeginContext(746, 195, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3e41208a4aff4380a8fe6d85fe6ee101", async() => {
                BeginContext(790, 144, true);
                WriteLiteral("\n        <input type=\"text\" name=\"groupName\" id=\"groupName\" class=\"form\">\n\n        <button class=\"btn btn-dialog btn-right\">Utwórz</button>\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(941, 149, true);
            WriteLiteral("\n\n    <a href=\"#\" onclick=\"closeDialog(\'newGroup\')\" class=\"close-button\"><span class=\"fas fa-times\"></span></a>\n</dialog>\n\n\n<div class=\"container\">\r\n");
            EndContext();
#line 34 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
     if (ViewBag.numberOfGroups == 0)
    {

#line default
#line hidden
            BeginContext(1136, 204, true);
            WriteLiteral("        <div class=\"no-groups\">\r\n            <span class=\"title\">Nie należysz do żadnej grupy</span>\r\n            <p>Możesz założyć własną grupę lub poprosić znajomych o zaproszenie.</p>\r\n        </div>\r\n");
            EndContext();
#line 40 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
    }
    else
    {

#line default
#line hidden
            BeginContext(1364, 319, true);
            WriteLiteral(@"    <table class=""table table-striped"">
        <thead class=""thead-dark"">
            <tr>
                <th width=""1"">
                </th>
                <th>
                    Nazwa grupy
                </th>
                <th width=""1""></th>
            </tr>
        </thead>
        <tbody>
");
            EndContext();
#line 55 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
             foreach (Group group in ViewBag.groups)
            {

#line default
#line hidden
            BeginContext(1752, 187, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        <span class=\"fas fa-users\"></span>\r\n                    </td>\r\n                    <td>\r\n                        <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1939, "\"", 1968, 2);
            WriteAttributeValue("", 1946, "/Drive/Index/", 1946, 13, true);
#line 62 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
WriteAttributeValue("", 1959, group.id, 1959, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1969, 1, true);
            WriteLiteral(">");
            EndContext();
            BeginContext(1971, 10, false);
#line 62 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
                                                    Write(group.name);

#line default
#line hidden
            EndContext();
            BeginContext(1981, 94, true);
            WriteLiteral("</a>\r\n                    </td>\r\n                    <td>\r\n                        <a href=\"#\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 2075, "\"", 2135, 5);
            WriteAttributeValue("", 2085, "openDialog(\'leaveGroup\',", 2085, 24, true);
            WriteAttributeValue(" ", 2109, "{\'groupId\':", 2110, 12, true);
            WriteAttributeValue(" ", 2121, "\'", 2122, 2, true);
#line 65 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
WriteAttributeValue("", 2123, group.id, 2123, 9, false);

#line default
#line hidden
            WriteAttributeValue("", 2132, "\'})", 2132, 3, true);
            EndWriteAttribute();
            BeginContext(2136, 91, true);
            WriteLiteral("><span class=\"fas fa-times\"></span></a>\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 68 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(2242, 32, true);
            WriteLiteral("        </tbody>\r\n    </table>\r\n");
            EndContext();
#line 71 "C:\Users\kamil\Documents\groupcloud\groupcloud\groupcloud\Views\Group\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(2281, 12, true);
            WriteLiteral("  \r\n</div>\n\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
