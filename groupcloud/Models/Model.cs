﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace groupcloud.Models
{
    public class GroupcloudContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Invitation> Invitations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=groupcloud.db");
        }
    }

    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public ICollection<Group> groups { get; set; }
        public bool groupAdministrator { get; set; }
        public bool globalAdministrator { get; set; }
    }

    public class Group
    {
        public int id { get; set; }
        public string name { get; set; }
        public ICollection<Catalog> catalogs { get; set; }
        public ICollection<File> files { get; set; }
    }

    public class Catalog
    {
        public int id { get; set; }
        public string name { get; set; }
        public ICollection<Catalog> catalogs { get; set; }
        public ICollection<File> files { get; set; }
        public User owner { get; set; }
    }

    public class File
    {
        public int id { get; set; }
        public string name { get; set; }
        public string fileExt{ get; set; }
        public User owner { get; set; }
        public DateTime creationDate { get; set; }
    }
    public class Invitation
    {
        public int id { get; set; }
        public string token { get; set; }
        public string userName { get; set; }
        public int groupId { get; set; }
        public string groupOwner { get; set; }
    }
}
