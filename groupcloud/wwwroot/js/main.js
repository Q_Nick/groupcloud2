function openDialog(Id, arg = []) {
    document.getElementById(Id).show()
    for (let key in arg) {
        if (arg.hasOwnProperty(key)) {
            document.getElementById(key).value = arg[key]
        }
    }
}

function closeDialog(Id) {
    document.getElementById(Id).close()
}

function checkValue() {
    const value = document.getElementById('input-file').value
    if (value != "") {
        console.log(value)
        return value
    }
}


const selector = document.getElementById('input-file')
selector.onchange = function() {
    const filename = selector.files[0].name
    document.getElementById('selectedFile').innerHTML = filename;
}