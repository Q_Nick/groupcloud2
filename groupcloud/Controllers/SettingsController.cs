﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using groupcloud.Models;

namespace groupcloud.Controllers
{
    public class SettingsController : BaseController
    {
        [HttpPost]
        public ActionResult ChangePassword(string oldPass, string newPass, string newPass2)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");

            var db = new GroupcloudContext();

            User user = GetUser(GetSessionItem("user"));

            if (user.password == oldPass)
            {
                if (newPass == newPass2)
                {
                    db.Users.Where(u => u.name == GetSessionItem("user")).ToList().First().password=newPass;
                    db.SaveChanges();
                    SetSessionItem("alertSuccess", "Hasło pomyślnie zmienione");
                }
                else
                {
                    SetSessionItem("alertDanger", "Podane hasła nie są identyczne");
                }
            }
            else
            {
                SetSessionItem("alertDanger", "Podane hasło jest błędne");
            }
            
            return RedirectToAction("Index", "Group");
        }
    }
}