﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using groupcloud.Models;
using System.Diagnostics;
using groupcloud.Controllers;
using Microsoft.EntityFrameworkCore;

namespace groupcloud.Controllers
{
    public class GroupController : BaseController
    {
        User user;
        public IActionResult Index()
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");
            ViewBag.alertDanger = GetAndDeleteSessionItem("alertDanger");
            ViewBag.alertSuccess = GetAndDeleteSessionItem("alertSuccess");

            var db = new GroupcloudContext();
            user= GetUser(GetSessionItem("user"));
            ViewBag.groups = user.groups.ToArray();
            ViewBag.numberOfGroups = user.groups.Count();
            
            ViewBag.user = user;
            ViewBag.usersAll = db.Users.ToArray();
            ViewBag.groupsAll = db.Groups.ToArray();

            return View();
        }
        [HttpPost]
        public ActionResult NewGroup(string groupName)
        {
            try
            {
                var db = new GroupcloudContext();
                

                Group group = new Group
                {
                    name = groupName
                };

                db.Groups.Add(group);

                User user = GetUser(GetSessionItem("user"));
                db.Users.Where(u => u.name == GetSessionItem("user")).Include(g=>g.groups).ToList().First().groups.Add(group);
                db.SaveChanges();
            }
            catch(System.Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: " + e.Message);
            }
            
            return RedirectToAction("Index", "Group");
        }
        [HttpPost]
        public ActionResult LeaveGroup(String sGroupId)
        {
            var db = new GroupcloudContext();

            try
            {
                int groupId = int.Parse(sGroupId);

                Group group = db.Groups.Where(g => g.id == groupId).ToArray().First();

                db.Users.Where(u => u.name == GetSessionItem("user")).Include(g => g.groups).ToArray().First().groups.Remove(group);
                db.SaveChanges();
                SetSessionItem("allertSuccess", "Pomyślnie opuszczono grupę");

            }
            catch(Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: " + e.Message);
            }


            return RedirectToAction("Index", "Group");

        }
    }
}