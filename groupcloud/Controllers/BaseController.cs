﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using groupcloud.Models;
using Microsoft.EntityFrameworkCore;

namespace groupcloud.Controllers
{
    public class BaseController : Controller
    {
        protected string GetAndDeleteSessionItem(string key)
        {
            string result=HttpContext.Session.GetString(key);
            HttpContext.Session.Remove(key);
            return result;
        }
        protected string GetSessionItem(string key)
        {
            return HttpContext.Session.GetString(key);
        }
        protected void SetSessionItem(string key, string value)
        {
            HttpContext.Session.SetString(key, value);
        }
        protected void RemoveSessionItem(string key)
        {
            HttpContext.Session.Remove(key);
        }
        protected User GetUser(string name)
        {
            var db = new GroupcloudContext();
            return db.Users.Where(u => u.name == name).Include(u=>u.groups).ToList().First();
        }
        protected bool IsSetUserSession()
        {
            if(GetSessionItem("user")!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}

