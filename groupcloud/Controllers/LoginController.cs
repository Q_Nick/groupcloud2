﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using groupcloud.Models;
using Microsoft.AspNetCore.Http;
using groupcloud.Controllers;

namespace groupcloud.Controllers
{
    public class LoginController : BaseController
    {
        public IActionResult Index()
        {
            ViewBag.incorrectName = GetAndDeleteSessionItem("incorrectName");
            ViewBag.incorrectPassword = GetAndDeleteSessionItem("incorrectPassword");
            ViewBag.nameError = GetAndDeleteSessionItem("nameError");
            ViewBag.passwordError = GetAndDeleteSessionItem("passwordError");
            ViewBag.emailError = GetAndDeleteSessionItem("emailError");
            ViewBag.newUser = GetAndDeleteSessionItem("newUser");

            return View();
        }

        [HttpPost]
        public ActionResult Login(User inputData)
        {
            var db = new GroupcloudContext();

            if (db.Users.Where(u => u.name==inputData.name).Count() != 0)
            {
                User selectedUser = db.Users.Where(u => u.name == inputData.name).ToList().First();
                if (selectedUser.password == inputData.password)
                {
                    SetSessionItem("user", inputData.name);
                    return RedirectToAction("Index", "Group");
                }
                else
                {
                    SetSessionItem("incorrectPassword", "Podano błędne hasło");
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                SetSessionItem("incorrectName", "Nie znaleziono użytkownika o podanej nazwie");
                return RedirectToAction("Index", "Login");
            }
        }

        [HttpPost]
        public ActionResult Register(User inputData)
        {
            bool correct = true;

            var db = new GroupcloudContext();

            if (inputData.name == null)
            {
                SetSessionItem("nameError", "Wprowadź nazwę użytkownika");
                correct = false;
            }

            if (db.Users.Where(u => u.name == inputData.name).Count()!=0)
            {
                SetSessionItem("nameError", "nazwa użytkownika jest już zajęta");
                correct = false;
            }

            if (inputData.password == null)
            {
                SetSessionItem("passwordError", "Wprowadź hasło");
                correct = false;
            }

            if (inputData.email == null)
            {
                SetSessionItem("emailError", "Wprowadź adres email");
                correct = false;
            }

            if (db.Users.Where(u => u.email == inputData.email).Count() != 0)
            {
                SetSessionItem("emailError", "Podany email jest już zajęty");
                correct = false;
            }

            if (correct == true)
            {
                User newUser = new User();
                newUser.name = inputData.name;
                newUser.password = inputData.password;
                newUser.email = inputData.email;

                db.Users.Add(newUser);
                db.SaveChanges();
                SetSessionItem("newUser", "Rejestracja zakończona, możesz zalogować się na nowe konto");
            }

            return RedirectToAction("Index", "Login");
        }

        public ActionResult Logout()
        {
            RemoveSessionItem("user");
            return RedirectToAction("Index", "Login");
        }
    }
}