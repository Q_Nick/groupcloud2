﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using groupcloud.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace groupcloud.Controllers
{
    public class DriveController : BaseController
    {
        

        public IActionResult Index(String urlGroup, String urlCatalog)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");
            ViewBag.alertDanger = GetAndDeleteSessionItem("alertDanger");
            ViewBag.alertSuccess = GetAndDeleteSessionItem("alertSuccess");

            var db = new GroupcloudContext();

            int selectedGroup;

            try
            {
                selectedGroup = int.Parse(urlGroup);
                if (urlCatalog != null)
                {
                    int catalogId = int.Parse(urlCatalog);
                    ViewBag.catalogId = db.Catalogs.Where(c => c.id == catalogId).ToList().First().id;
                    ViewBag.catalogs = db.Catalogs.Where(c => c.id == catalogId).Include(c => c.catalogs).ToList().First().catalogs;
                    ViewBag.files = db.Catalogs.Where(c => c.id == catalogId).Include(c=>c.files).ToList().First().files.ToList();
                }
                else
                {
                    ViewBag.catalogId = null;
                    ViewBag.catalogs = db.Groups.Where(g => g.id == selectedGroup).Include(c => c.catalogs).ToList().First().catalogs;
                    ViewBag.files = db.Groups.Where(g => g.id == selectedGroup).Include(g=>g.files).ToList().First().files.ToList();
                }
            }
            catch(Exception e)
            {
                SetSessionItem("alertDanger", "Wybrana grupa nie istnieje "+e.Message);
                return RedirectToAction("Index", "Group");
            }


            ViewBag.user = GetUser(GetSessionItem("user"));
            ViewBag.group = db.Groups.Where(g => g.id == selectedGroup).ToList().First();

            return View();
        }
        [HttpPost]
        public IActionResult NewFile(String urlGroup, String urlCatalog, IFormFile uploadedFile)
        {

            try
            {
                var db = new GroupcloudContext();

                int groupId = int.Parse(urlGroup);

                File file = new File()
                {
                    name = uploadedFile.FileName,
                    fileExt= System.IO.Path.GetExtension(uploadedFile.FileName).ToLowerInvariant(),
                    owner = db.Users.Where(u => u.name == GetSessionItem("user")).ToList().First(),
                    creationDate = DateTime.Now
                };

                if (urlCatalog != null)
                {
                    int catalogId = int.Parse(urlCatalog);
                    db.Catalogs.Where(c => c.id == catalogId).Include(c => c.files).ToList().First().files.Add(file);
                }
                else
                {
                    db.Groups.Where(g => g.id == groupId).Include(g => g.files).ToList().First().files.Add(file);
                }

                db.Files.Add(file);
                db.SaveChanges();
                String newFileName = db.Files.Max(f=>f.id).ToString();

                var path = System.IO.Path.Combine(
                  System.IO.Directory.GetCurrentDirectory(), "wwwroot/upload",
                  newFileName+file.fileExt);

                using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Create))
                {
                    uploadedFile.CopyToAsync(stream);
                }

                db.SaveChanges();
                return RedirectToAction("Index", "Drive", new { urlGroup=groupId });
            }
            catch (Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: " + e.Message);
                return RedirectToAction("Index", "Group");
            }

        }
        public void EditFile()
        {

        }
        public void RemoveFile()
        {

        }
        public ActionResult NewCatalog(string groupId, string currentCatalog, string catalogName)
        {

            var db = new GroupcloudContext();

            try
            {
                int _groupId = int.Parse(groupId);

                Catalog catalog = new Catalog
                {
                    name = catalogName,
                    owner = db.Users.Where(u => u.name == GetSessionItem("user")).ToList().First()
                };

                if (currentCatalog == null)
                {
                    db.Groups.Where(g => g.id == _groupId).Include(c=>c.catalogs).ToList().First().catalogs.Add(catalog);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Drive", new { urlGroup = _groupId});
                }
                else
                {
                    int _currentCatalog = int.Parse(currentCatalog);
                    db.Catalogs.Where(g => g.id == _currentCatalog).Include(c => c.catalogs).ToList().First().catalogs.Add(catalog);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Drive", new { urlGroup = _groupId, urlCatalog = _currentCatalog });
                }

            }
            catch(Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: " + e.Message);
                return RedirectToAction("Index", "Group");
            }
        }
        public async Task<IActionResult> DownloadFile(string urlFileId)
        {
            if (urlFileId == null)
                return Content("filename not present");

            var db = new GroupcloudContext();

            File file = db.Files.Where(f => f.id == int.Parse(urlFileId)).ToList().First();

            var path = System.IO.Path.Combine(
                           System.IO.Directory.GetCurrentDirectory(),
                           "wwwroot/upload", urlFileId+file.fileExt);

            var memory = new System.IO.MemoryStream();
            using (var stream = new System.IO.FileStream(path.ToString(), System.IO.FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            var resultFile= File(memory, GetContentType(path), System.IO.Path.GetFileName(path));
            return resultFile;
        }
        public void EditCatalog()
        {

        }
        public void RemoveCatalog()
        {

        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = System.IO.Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}