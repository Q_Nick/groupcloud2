﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using groupcloud.Models;
using Microsoft.EntityFrameworkCore;

namespace groupcloud.Controllers
{
    public class InvitationController : BaseController
    {
        public IActionResult Index(string urlGroup)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");

            try
            {
                var db = new GroupcloudContext();

                Invitation inv = db.Invitations.Where(i => i.token == urlGroup).ToList().First();
                ViewBag.inv = inv;
                ViewBag.user = GetUser(GetSessionItem("user"));
                ViewBag.groupName = db.Groups.Where(
                    g => g.id == inv.groupId
                    ).ToList().First().name;
                ViewBag.hostName = Request.Host.Value;

                return View();
            }
            catch(Exception e)
            {
                SetSessionItem("alertDanger", "Podane zaproszenie nie istnieje lub jest nieaktualne");
                return RedirectToAction("Index", "Group");
            }

        }
        public IActionResult New(string userName, string groupId)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");

            var db = new GroupcloudContext();
            Invitation invitation = new Invitation
            {
                token = Convert.ToBase64String(Encoding.Unicode.GetBytes(DateTime.Now.ToString("yyyyMMddHHmmss"))),
                userName = userName,
                groupId = int.Parse(groupId),
                groupOwner = GetUser(GetSessionItem("user")).name
            };

            db.Invitations.Add(invitation);
            db.SaveChanges();

            ViewBag.invitationLink = "https://"+Request.Host.Value+ "/Invitation/Index/" + invitation.token;
            ViewBag.user = GetUser(GetSessionItem("user"));

            return View();
        }
        public IActionResult Accept(string urlGroup)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");

            try
            {
                var db = new GroupcloudContext();

                int gpId = db.Invitations.Where(i => i.token == urlGroup).ToList().First().id;
                Group gp = db.Groups.Where(g => g.id == gpId).ToList().First();
                db.Users.Where(u => u.name == GetSessionItem("user")).Include(g=>g.groups).ToList().First().groups.Add(gp);

                db.Invitations.Remove(db.Invitations.Where(i => i.token == urlGroup).ToList().First());
                db.SaveChanges();

                return RedirectToAction("Index", "Group");
            }
            catch (Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: "+e.Message);
                return RedirectToAction("Index", "Group");
            }
        }
        public IActionResult Deny(string urlGroup)
        {
            if (!IsSetUserSession())
                return RedirectToAction("Index", "Login");

            try
            {
                var db = new GroupcloudContext();

                db.Invitations.Remove(db.Invitations.Where(i => i.token == urlGroup).ToList().First());
                db.SaveChanges();

                return RedirectToAction("Index", "Group");
            }
            catch (Exception e)
            {
                SetSessionItem("alertDanger", "Wystąpił błąd: " + e.Message);
                return RedirectToAction("Index", "Group");
            }
        }
    }
}