﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace groupcloud.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Invitations",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    token = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true),
                    groupId = table.Column<int>(nullable: false),
                    groupOwner = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invitations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    groupAdministrator = table.Column<bool>(nullable: false),
                    globalAdministrator = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(nullable: true),
                    Userid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.id);
                    table.ForeignKey(
                        name: "FK_Groups_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Catalogs",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(nullable: true),
                    ownerid = table.Column<int>(nullable: true),
                    Catalogid = table.Column<int>(nullable: true),
                    Groupid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogs", x => x.id);
                    table.ForeignKey(
                        name: "FK_Catalogs_Catalogs_Catalogid",
                        column: x => x.Catalogid,
                        principalTable: "Catalogs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Catalogs_Groups_Groupid",
                        column: x => x.Groupid,
                        principalTable: "Groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Catalogs_Users_ownerid",
                        column: x => x.ownerid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Files",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(nullable: true),
                    fileExt = table.Column<string>(nullable: true),
                    ownerid = table.Column<int>(nullable: true),
                    creationDate = table.Column<DateTime>(nullable: false),
                    Catalogid = table.Column<int>(nullable: true),
                    Groupid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Files", x => x.id);
                    table.ForeignKey(
                        name: "FK_Files_Catalogs_Catalogid",
                        column: x => x.Catalogid,
                        principalTable: "Catalogs",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Files_Groups_Groupid",
                        column: x => x.Groupid,
                        principalTable: "Groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Files_Users_ownerid",
                        column: x => x.ownerid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Catalogs_Catalogid",
                table: "Catalogs",
                column: "Catalogid");

            migrationBuilder.CreateIndex(
                name: "IX_Catalogs_Groupid",
                table: "Catalogs",
                column: "Groupid");

            migrationBuilder.CreateIndex(
                name: "IX_Catalogs_ownerid",
                table: "Catalogs",
                column: "ownerid");

            migrationBuilder.CreateIndex(
                name: "IX_Files_Catalogid",
                table: "Files",
                column: "Catalogid");

            migrationBuilder.CreateIndex(
                name: "IX_Files_Groupid",
                table: "Files",
                column: "Groupid");

            migrationBuilder.CreateIndex(
                name: "IX_Files_ownerid",
                table: "Files",
                column: "ownerid");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_Userid",
                table: "Groups",
                column: "Userid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Files");

            migrationBuilder.DropTable(
                name: "Invitations");

            migrationBuilder.DropTable(
                name: "Catalogs");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
